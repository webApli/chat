/**
 * GruposController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    save:async function (req, res) {
        const nombre=req.param('nombre');
        const q=await Grupo.getDatastore().sendNativeQuery('select max(id) as id from Grupo',[]);
        const id=q.rows[0].id+1;
        const grupo=await Grupo.create({
         id:id
         ,nombre:nombre});
        res.status(200).json(grupo);
     },
     get: async function (req, res) {
         const  grupo=await Grupo.find();
         res.status(200).json(grupo);
     },

};

