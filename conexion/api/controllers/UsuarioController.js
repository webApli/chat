/**
 * UsuarioController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    findByUsuarioPassword:async function (req, res) {
        const username=req.param('username');
        const password=req.param('password');
        const  usuario=await Usuario.find({username:username,password:password});
        res.status(200).json(usuario);
     },
    save:async function (req, res) {
        const username=req.param('username');
        const password=req.param('password');
        const q=await Usuario.getDatastore().sendNativeQuery('select max(id) as id from Usuario',[]);
        console.log(q);
        const id=q.rows[0].id+1;
        console.log(id);
        const usuario=await Usuario.create({
         id:id
         ,username:username
         ,password:password});
        res.status(200).json(usuario);
     },
     get: async function (req, res) {
         const  usuario=await Usuario.find();
         res.status(200).json(usuario);
     },

};

