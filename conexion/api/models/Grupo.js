/**
 * Grupo.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id:{
      type:'number',
      required: true,
      columnType: 'INT'
    },
    nombre:{
      type:'string',
      required: true,
      columnType: 'VARCHAR(45)'
    },
  }

};

