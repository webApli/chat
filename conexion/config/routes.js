/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  // usuario
  'get /usuarios':'UsuarioController.get',
  'get /usuarios/:username/:password':'UsuarioController.findByUsuarioPassword',
  'get /registroUsuario/:username/:password':'UsuarioController.save',
  // grupos
  'get /grupos':'GruposController.get',
  'get /registroGrupo/:nombre':'GruposController.save',
};
