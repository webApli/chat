const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const io = require('socket.io')(server);
const base64url = require('base64url');
var request = require('request');

var listMessagesGeneral=[];//mensaje
var listMessagesIndividual=[];//usuarioDesde,usuario hasta,mensaje
var listMessagesGrupal=[];//id grupo, usuario, mensaje,
var lobbys=[];
var listUsuarios=[];
var listSokets=[];

app.use(express.static('public'));
  app.get('/', function(req, res){
    res.sendFile(`${__dirname}/public/views/index.html`, (e) => {
      
    });
  });
  app.get('/registrarme/:usuario',async function(req, res){
    const usuarioPassword=req.params.usuario.split('--');
    const usuario=usuarioPassword[0];
    const clave=usuarioPassword[1];
    await request(`http://localhost:1337/registroUsuario/${usuario}/${clave}`
    , function (error, response, body) {
        res.sendFile(`${__dirname}/public/views/index.html`, (e) => {});
    });
  });
  app.get('/chatroom/:username',async function(req, res){
    const usuarioPassword=req.params.username.split('--');
    const usuario=usuarioPassword[0];
    const clave=usuarioPassword[1];
    await request(`http://localhost:1337/usuarios/${usuario}/${clave}`
    , function (error, response, body) {
      if ((response && response.statusCode)==200) {
        if (JSON.parse(body)[0]!=null) {
          res.sendFile(`${__dirname}/public/views/chatroom.html`, (e) => {});
        }else{
          res.sendFile(`${__dirname}/public/views/index.html`, (e) => {});
        }
      }else{
        res.sendFile(`${__dirname}/public/views/index.html`, (e) => {});
      }
    });
  });

io.on('connection', function(socket){
  // ################
  //Manejo de conexion y desconexion de usuario 
  // ################
  socket.on('disconnect', function(user){
    for (let i = 0; i < listUsuarios.length; i++) {
      const usu = listUsuarios[i];
      if (usu.id==socket.id) {
        listUsuarios.splice(i, 1);
        listSokets.splice(i, 1);
      }
    }
    socket.disconnected;
  });
  socket.on('username', function(username,password) {
    socket.username = username;
    listUsuarios.push({'id':socket.id,'username':username,'password':password});
    listSokets.push(socket);
    io.emit('listUsuarios',listUsuarios);
    io.to (socket.id) .emit('lobbys',lobbys);
  });
  // ################
  //  retorno de lista
  // ################
  socket.on('getLista', function(type) {
    if (type=='grou') {
      io.to (socket.id) .emit ('listMessages' , listMessagesGrupal);
    }else if (type=='indi') {
      io.to (socket.id) .emit ('listMessages' , listMessagesIndividual);
    }else if (type=='gene') {
      io.to (socket.id) .emit ('listMessages' , listMessagesGeneral);
    }
  });
  // ################
  //envio de mensajes
  // ################
  socket.on('sendMessage', function(message) {
    // socket.broadcast.emit ( 'mensajeEnviado', message);
    io.emit ('menssageEnviadoGeneral' , socket.username+'=>'+message);
    listMessagesGeneral.push({mensaje:socket.username+'=>'+message});
  });
  socket.on ( 'chatIndividual' , function ( id, msg ) {
    let mensaje={usuarioDesde:socket.id,usuarioHasta:id ,mensaje:socket.username+'=>'+msg}
    socket.to (id) .emit ( 'menssageEnviadoPrivado' , mensaje);
    listMessagesIndividual.push(mensaje);
  } ); 
  socket.on ( 'chatGrupal' , function ( grupo, msg ) {
    io.to(grupo).emit('menssageEnviadoGrupal',socket.username+'=>'+msg);
    listMessagesGrupal.push({
      grupo:grupo
      ,usuario:socket.id
      ,mensaje:socket.username+'=>'+msg
    });
  } ); 
  // ################
  //  Manejo de grupos 
  // ################
  socket.on ( 'conectarGrupo' , function ( grupo ) { 
    socket.join ( grupo ); 
    io.to(grupo).emit('ingreso','ingreso a grupo '+ socket.username);
  } );
  socket.on ( 'crearGrupo' , async function (nombre,integrantes) { 
    const usuarios = integrantes.split(",");
    if (!lobbys.includes('/'+nombre)) {
      //SE CREA EL GRUPO
      const nsp=io.of ( '/'+nombre );
      nsp.on( 'conexión' , function (socket) {
      }); 
      //SE CONECTA EL USUARIO CREADOR Y DEMAS INTEGRANTES
      socket.join ( '/'+nombre );
      io.to( '/'+nombre ).emit ('conexionGrupo' , `se conecto  al grupo: ${socket.username}` );
      if (usuarios.length>0) { 
        for (let i = 0; i < usuarios.length; i++) {
          if (usuarios[i]!= null && usuarios[i].length>0) {
            for (let j = 0; j < listUsuarios.length; j++) {
              if (listUsuarios[j].username==usuarios[i]) {
                console.log(usuarios[i]);
                listSokets[j].join ( '/'+nombre );
                io.to( '/'+nombre ).emit ('conexionGrupo' , `se conecto  al grupo: ${listUsuarios[j].username}` );
              }
            }
          }
        }
      }
      await request(`http://localhost:1337/registroGrupo/${nombre}`, function (error, response, body) {});
      lobbys.push('/'+nombre);//{nombre:'/'+nombre,idGrupo: });
      io.emit('lobbys',lobbys);
    }
  } ); 
    
});

// function cargarUsuarios() {
//   request(`http://localhost:1337/usuarios`, function (error, response, body) {
//     console.log('usuarios'+body)
//     if ((response && response.statusCode)==200) {
//       for (const i of JSON.parse(body)) {
//         listUsuarios.push({'id':i.id,'username':i.username,'password':i.password});
//       }
//     }
//   });
// }

async function cargarGrupos() {
  await request(`http://localhost:1337/grupos`, function (error, response, body) {
    if ((response && response.statusCode)==200) {
      for (const i of JSON.parse(body)) {
        lobbys.push('/'+i.nombre);
      }
    }
  });
}

server.listen(3000, function(){
  console.log('listening on *:3000');
  cargarGrupos();
});