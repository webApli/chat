// var request = require('request');
var socket;
var usersList = document.querySelector("#listUsers");
var groupsList = document.querySelector("#listGroups");
var listMessage = document.querySelector("#listMessage");
var listUsers = document.querySelector("#users");
var listGrupos = document.querySelector("#grupos");
var chatType = document.querySelector("#chatType");
let botomGo=document.querySelector('#botomGo');
let mensaje=document.querySelector('#message');
let myUser=document.querySelector('#myUser');
let type='gene';
let grupoActual='';
var users = [];
let user;
let usuarioMsgPriva;

function connect() {
    user = getUser();
    if (user == null) {
        alert('No se encontro un nombre de usuario');
        return;
    }
    myUser.innerHTML=user;
    //##################
    //manejo de conexion
    //##################
    if (socket) { socket.disconnect(this.usuario); }
    socket = io();
    socket.emit('username', user);
    //##################
    //GRUPOS
    //##################
    socket.on('conexionGrupo', (mensaje) => {
        console.log(mensaje);
    });
    socket.on('ingreso', (mensaje) => {
        console.log(mensaje);
    });
    socket.on('lobbys', (lobbys) => {
        listGrupos.innerHTML='';
        for (const men of lobbys) {
            const el = document.createElement("li");
            el.className='ms';
            el.innerHTML=men;
            el.onclick=()=>unirGrupo(men);
            addEventColor(el);
            listGrupos.appendChild(el);   
        }
    });
    //##################
    //Usuario
    //##################
    socket.on('listUsuarios', (listUsuarios) => {
        listUsers.innerHTML='';
        for (const usu of listUsuarios) {
            if (usu.id!=socket.id) {
                const el = document.createElement("div");
                const imagen = document.createElement("img");
                imagen.src='../assets/user.png';
                el.appendChild(imagen);
                const lbl = document.createElement("label");
                lbl.innerHTML=usu.username;
                el.appendChild(lbl);
                addEventColor(el);
                addEventChatIndv(el,usu);
                el.className='ms';
                listUsers.appendChild(el);   
            }
        }
    });
    //##################
    //Mensajes
    //##################
    socket.on('menssageEnviadoGeneral',(men)=>{
        if (type=='gene') {
            agregarMenssage(men);
        }
    });
    socket.on('menssageEnviadoPrivado',(men)=>{
        if (type=='indi') {
            agregarMenssage(men);
        }
    });
    socket.on('menssageEnviadoGrupal',(men)=>{
        if (type=='grou') {
            agregarMenssage(men);
        }
    });
    socket.on('listMessages',(lista)=>{
        cargarListaMensajes(lista); 
    });
}
//##########################
//ENVIO DE MENSAJES
//##########################
function sendMessage(){
    if (mensaje.value!='' && user!=null) {
        socket.emit('sendMessage',mensaje.value);
        mensaje.value='';
    }
}
function sendMsgIndividual(id) {
    if (mensaje.value!='') {
        socket.emit('chatIndividual',id,mensaje.value);
        agregarMenssage(user+'=>'+mensaje.value);
        mensaje.value='';
    }
}
function sendMsgGroup (grupo) {
    if (mensaje.value!='') {
        socket.emit('chatGrupal',grupo,mensaje.value);
        mensaje.value='';
    }
}

//##########################
//GRUPOS
//##########################

function createGroup(){
    let nombreGrupo = prompt("Ingrese el nombre del grupo", "");
    let integrantes=prompt("Ingrese el numero del usuario separado por (,)", "");
    socket.emit('crearGrupo',nombreGrupo,integrantes);
}
function unirGrupo(nombre) {
    grupoActual=nombre;
    socket.emit('conectarGrupo',nombre);
    addEventChatGroup(nombre);
}
function grupoGeneral() {
    addEventChat();
}

//##########################
//ADD EVENTOS
//##########################
function addEventColor(elem){
    let lista=document.getElementsByClassName('ms');
    elem.addEventListener('click',()=>{
        for (const el of lista) {
            el.style.background='#f8f8f8';
        }
        elem.style.background='#ebebeb';
    })
}
function addEventChatIndv(elemento,usuario) {
    elemento.addEventListener('click',()=>{
        type='indi';
        usuarioMsgPriva=usuario.id;
        socket.emit('getLista',type);
        chatType.innerHTML="Chat con "+usuario.username;
        document.querySelector('#botomGo').onclick=()=>{sendMsgIndividual(usuario.id);};
    });
}
function addEventChatGroup(grupo) {
    type='grou';
    chatType.innerHTML="Chat de grupo = "+grupo;
    socket.emit('getLista',type);
    document.querySelector('#botomGo').onclick=()=>{sendMsgGroup(grupo);};
}
function addEventChat() {
    type='gene';
    chatType.innerHTML="Chat General";
    socket.emit('getLista',type);
    document.querySelector('#botomGo').onclick=()=>{sendMessage();};
}

function cargarListaMensajes(lista){
    listMessage.innerHTML='';
    console.log('type')
    for (const men of lista) {
        if (type=='grou') {
            if (men.grupo.includes(grupoActual)) {
                const el = document.createElement("div");
                el.className='mensaje';
                el.innerHTML=men.mensaje;
                listMessage.appendChild(el);   
            }
        }else if(type=='indi'){
            if ((socket.id==men.usuarioDesde || socket.id==men.usuarioHasta)
                && (usuarioMsgPriva==men.usuarioDesde || usuarioMsgPriva==men.usuarioHasta)) {
                const el = document.createElement("div");
                el.className='mensaje';
                el.innerHTML=men.mensaje;
                listMessage.appendChild(el);   
            }
        }else{
            const el = document.createElement("div");
            el.className='mensaje';
            el.innerHTML=men.mensaje;
            listMessage.appendChild(el);   
        }
    }
}
function agregarMenssage(men) {
    console.log(typeof men);
    if (type=='indi' && typeof men=='object') {
        if ((socket.id==men.usuarioDesde || socket.id==men.usuarioHasta)
        && (usuarioMsgPriva==men.usuarioDesde || usuarioMsgPriva==men.usuarioHasta)){
            const el = document.createElement("div");
            el.className='mensaje';
            el.innerHTML=men.mensaje;
            listMessage.appendChild(el);   
        }
    }else{
        const el = document.createElement("div");
        el.className='mensaje';
        el.innerHTML=men;
        listMessage.appendChild(el);
    }
}

function mostrarUsuario() {
    usersList.style.display=usersList.style.display=='flex'?'none':'flex';
}
function mostrarGrupos() {
    groupsList.style.display=groupsList.style.display=='flex'?'none':'flex';
}

function getUser() {
    const vars=window.location.href.split('/');
    return vars[4].split('--')[0];
}


const as=document.querySelectorAll('.ms');
for (const a of as) {
    addEventColor(a);
}
botomGo.addEventListener('click',sendMessage());